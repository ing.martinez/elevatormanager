﻿using ServicesBus.HandlerAzureServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilNuget.Enum;

namespace ServiceApplication.Models.Queue
{
    public class QueueInputDto
    {
        public int Floor { get; set; }
    }
}
