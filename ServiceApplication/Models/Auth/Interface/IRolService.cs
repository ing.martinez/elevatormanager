﻿
using Domain.Entities;
using ServiceApplication.Dto;

namespace ServiceApplication
{
    public interface IRolService : IBaseServiceApplication<Rol, RolDto>
    {

    }
}
