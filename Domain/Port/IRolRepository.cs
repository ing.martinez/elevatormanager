﻿using Domain.Entities;

namespace Domain.Port
{
    public interface IRolRepository : IRepositoryBase<Rol>
    {
    }
}
