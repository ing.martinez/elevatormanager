﻿using System;
using Domain.Entities;

namespace Domain.Port
{
	public interface IElevatorMovementRepository: IRepositoryBase<ElevatorMovement>
    {
	}
}

