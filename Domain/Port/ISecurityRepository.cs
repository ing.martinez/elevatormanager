﻿
using Domain.Entities;

namespace Domain.Port
{
    public interface ISecurityRepository : IRepositoryBase<User>
    {
    }
}
