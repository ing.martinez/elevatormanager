﻿using System;
using Domain.Entities;

namespace Domain.Port
{
	public interface IElevatorRepository: IRepositoryBase<Elevator>
    {
	}
}

