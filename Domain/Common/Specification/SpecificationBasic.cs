﻿using Domain.Specification;

namespace Domain.Common.Specification
{
    public class SpecificationBasic<T> : SpecificationBase<T> where T : BaseEntity
    {
    }
}
