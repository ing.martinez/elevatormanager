﻿namespace Domain.Common
{
    public record Metadata(string LogicName, string Tag, int Length, int MinLength = 1);
}
