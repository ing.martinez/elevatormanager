﻿namespace Domain.Common
{
    public enum States
    {
        Active,
        Inactive,
        Send,
        Receive,
        Process,
        InUse,
        Finished,
        Complete

    }
}
